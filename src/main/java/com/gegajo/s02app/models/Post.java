package com.gegajo.s02app.models;

import javax.persistence.*;

@Entity
@Table(name="posts")
public class Post {
    //Properties(columns)
    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String title;

    @Column
    private String content;

    //Constructor
    //Data: title, content
    //Empty
    public Post() {}

    //Parameterized
    public Post(String title, String content){
        this.title = title;
        this.content = content;
    }
    //Getters & Setters
    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setId(Long id) {
        this.id = id;
    }

    //Methods

}
